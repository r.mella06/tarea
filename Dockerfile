FROM openjdk:17-ea-8-jdk-slim
COPY "./target/demo-0.0.1-SNAPSHOT.jar" "TareaDocker.jar"
EXPOSE 8080
ENTRYPOINT ["java","-jar","TareaDocker.jar"]