package com.example.demo.models;

public class Reponedor extends Usuario {

    private String seccion;

    public Reponedor(String nombre, String rut, String contraseña, String seccion) {
        super(nombre, rut, contraseña);
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }
}
