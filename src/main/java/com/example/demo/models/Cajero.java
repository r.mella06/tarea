package com.example.demo.models;

public class Cajero extends Usuario{

    private int ventas;

    public Cajero(String nombre, String rut, String contraseña,int ventas) {
        super(nombre, rut, contraseña);
    }

    public int getVentas() {
        return ventas;
    }

    public void setVentas(int ventas) {
        this.ventas = ventas;
    }
}
