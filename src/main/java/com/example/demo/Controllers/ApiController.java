package com.example.demo.Controllers;

import com.example.demo.models.Cajero;
import com.example.demo.models.Merma;
import com.example.demo.models.Producto;
import com.example.demo.models.Reponedor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Array;
import java.util.ArrayList;

@RestController
public class ApiController {

    @GetMapping("/productos")
    //8 productos
    public ArrayList<Producto> obtenerProductos(){
        ArrayList<Producto> productos = new ArrayList<>();
        Producto p1 = new Producto("Coca-Cola","B8",18,2000,"Fila 1");
        Producto p2 = new Producto("Cereal","P2",20,2250,"Fila 2");
        Producto p3 = new Producto("Queso Gauda","G1",30,4100,"Fila 3");
        Producto p4 = new Producto("Pañales", "F2", 25, 22000, "Fila 4");
        Producto p5 = new Producto("Charquican Picado", "F2", 19, 1830, "Fila 4");
        Producto p6 = new Producto("Pack Cerveza Lager", "C1", 12, 6250, "Fila 5");
        Producto p7 = new Producto("Sofrito Congelado", "C2", 12, 490, "Fila 6");
        Producto p8 = new Producto("Papas Duquesas", "P1", 3, 3190 , "Fila 7");
        productos.add(p1);
        productos.add(p2);
        productos.add(p3);
        productos.add(p4);
        productos.add(p5);
        productos.add(p6);
        productos.add(p7);
        productos.add(p8);
        return productos;
    }

    @GetMapping("/mermas")
    public ArrayList<Merma> obtenerMerma(){
        ArrayList<Merma> mermas = new ArrayList<>();
        Merma m1 = new Merma("Queso Gauda","Robo de Producto", 10);
        Merma m2 = new Merma("Charquican Picado", "Poca venta ", 80);
        Merma m3 = new Merma("Papas Duqeusas", "Perdida de valor", 40);
        mermas.add(m1);
        mermas.add(m2);
        mermas.add(m3);

        return mermas;
    }

    @GetMapping("/reponedores")
    public ArrayList<Reponedor> obtenerReponedor(){
        ArrayList<Reponedor> reponedores = new ArrayList<>();
        Reponedor r1 = new Reponedor("Pedro","11.111.111-1", "Pedro123", "Licores");
        Reponedor r2 = new Reponedor("Jorge", "22.222.222-2", "Jorge123", "Congelados");
        Reponedor r3 = new Reponedor("Nitales", "33.333.333-3", "Nitales123", "Colasiones");
        reponedores.add(r1);
        reponedores.add(r2);
        reponedores.add(r3);
        return reponedores;
    }

    @GetMapping("/cajeros")
    public ArrayList<Cajero> obtenerCajero(){
        ArrayList<Cajero> cajeros = new ArrayList<>();
        Cajero c1 = new Cajero("Cesar", "44.444.444-4", "Nitales123", 14);
        Cajero c2 = new Cajero("Daniel", "55.555.555-5","Daniel123", 25);
        Cajero c3 = new Cajero("Carmen", "66.666.666-6", "Carmen123", 43);
        Cajero c4 = new Cajero("Fernando", "77.777.777-7", "Fernando123", 30);
        Cajero c5 = new Cajero("Francisco", "88.888.888-8", "Francisco123", 32);
        cajeros.add(c1);
        cajeros.add(c2);
        cajeros.add(c3);

        return cajeros;

    }


}
